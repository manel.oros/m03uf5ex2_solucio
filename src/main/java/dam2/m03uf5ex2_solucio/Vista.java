/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam2.m03uf5ex2_solucio;

import java.util.Map;
import java.util.Scanner;

/**
 * Gestiona totes les interaccions amb l'usuari via consola
 * @author manel
 */
public class Vista {
    
    /***
     * Retorna l'opció seleccionada per l'usuari al menú principal
     * @param _d Model amb el qual es treballa
     * @return 
     */
    public static int obtenirOpcioMenuPrincipal(Model _d)
    {
        boolean sortir = false;
        int ret = 5;
        
        System.out.println();
        System.out.println("Diccionari " + _d.toString());
        System.out.println("-------------------------");
        System.out.println("1 - Afegir entrada");
        System.out.println("2 - Eliminar entrada");
        System.out.println("3 - Mostrar traducció");
        System.out.println("4 - Llistar entrades");
        System.out.println("5 - Sortir");
        
        //demanem mentre no ens posin l'opció correcta
        while (!sortir)
        {
            Scanner myObj = new Scanner(System.in);  
            System.out.println("Seleccionar una opció: ");
            
            if (myObj.hasNextInt())
            {
                ret = myObj.nextInt();
               
                if (ret < 1 || ret > 5)
                {
                   System.out.println("Opció incorrecta.");
                }
                else
                {
                    sortir = true;
                } 
            }
        }
        
        return ret;
    }
    
    /**
     * Demana i retorna les entrades per l'usuari. S'ha de controlar que si l'antrada és en blanc, no fer res.
     * @param id idioma
     * @return 
     */
    public static String demanarEntrada(enum_Idiomes id)
    {
        String ret;
        boolean sortir = false;
        
        System.out.println("Introduïr paraula en "+ id.toString());
        Scanner myObj = new Scanner(System.in);
        ret = myObj.nextLine();
        
        return ret;
    }
    
    /***
     * Retorna un text amb la paraula original i la traducció
     * @param clau
     * @param valor 
     */
    public static void mostrarTraduccio(String clau, String valor)
    {
         System.out.println("Clau:" + clau + " Valor:" + valor);
    }
    
    /***
     * Mostra un missatge genèric de sortida
     */
    public static void missatgeDeSortida()
    {
        System.out.println("Bye!");
    }
    
    /***
     * Mostra un missatge d'error genèric
     */
    public static void mostraError()
    {
        System.out.println("ERROR: L'operació no s'ha pogut realitzar correctament");
    }
    
    /***
     * Mostra una operació amb resultat correcte
     */
    public static void mostraOperacioOk()
    {
        System.out.println("OK!");
    }
    
    /***
     * Mostra un llistat de totes les entrades del diccionari
     * @param d 
     */
    public static void llistarEntrades(Model d)
    {
        System.out.println("Listant entrades del diccionari " + d);
        
        // aplica un format de dues columnes, de 20 espais per columna, 
        // amb un separador ":" i finalmentun salt de linia
        String format = "%1$-20s : %2$-20s \n";
        
        for (Map.Entry<String, String> entry : d.getDades().entrySet()) 
        {
            System.out.format(format, entry.getKey(), entry.getValue());   
        }
        System.out.println("Total: " + d.totalEntrades() + " entrada/es");
    }
}
