/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam2.m03uf5ex2_solucio;

/**
 * Enumeració per a definir els diversos idiomes amb els que treballa el diccionari
 * 
 * @author manel
 */
public enum enum_Idiomes {
    
    CAT,
    ANG
    
}
