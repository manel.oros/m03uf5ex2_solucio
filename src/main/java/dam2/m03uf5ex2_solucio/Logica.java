/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam2.m03uf5ex2_solucio;

/**
 * Gestiona la lógica de gestió del diccionari
 * @author manel
 * Classe inicial 
 * Darrera rev 9/23
 */
public class Logica {

    /**
     * Punt d'entrada de l'aplicació
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // semàfor que finalitza l'aplicació
        boolean fi = false;
        
        // diccionari
        Model diccionari = new Model();
        
        // bucle principal
        while (!fi)
        {
            // obtenim la opció escollida per l'usuari
            int opc = Vista.obtenirOpcioMenuPrincipal(diccionari); 
            
            // analitzem opció escollida per l'usuari
            switch (opc)
            {
                //sortir
                case 5:{ fi= true; 
                        Vista.missatgeDeSortida();
                        break;}
                
                //afegir
                case 1:{ String clau = Vista.demanarEntrada(enum_Idiomes.CAT);
                         String valor = Vista.demanarEntrada(enum_Idiomes.ANG);
                         if (clau.isBlank() || valor.isBlank())
                             Vista.mostraError();
                         else
                         {
                            diccionari.afegirParaula(clau, valor);
                            Vista.mostraOperacioOk();
                         }
                         break;}
                //eliminar
                case 2:{  String clau = Vista.demanarEntrada(enum_Idiomes.CAT);
                          boolean eliminat = diccionari.eliminarParaula(clau);
                          if (!eliminat)
                              Vista.mostraError();
                          else 
                              Vista.mostraOperacioOk();
                          break;}
                //mostrar
                case 3:{  String clau = Vista.demanarEntrada(enum_Idiomes.CAT);
                          String ang = diccionari.cercarTraduccio(clau);
                          if (ang != null)
                            Vista.mostrarTraduccio(clau, ang);
                          else
                            Vista.mostraError();
                    break;}
                
                //llistar entrades
                case 4:{ Vista.llistarEntrades(diccionari);
                        break;}
            }
        }
        
        // finalitzem sense errors
        System.exit(0);
    }
    
}
