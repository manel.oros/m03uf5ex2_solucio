package dam2.m03uf5ex2_solucio;

import java.util.Map;
import java.util.TreeMap;


/***
 * Implementa un diccionari de traducció i tots els seus mètodes de gestió de dades
 * @author manel
 */
public class Model {

        /***
         * Estructura que emmagatzema els parells clau-valor.
         * Atenció: MAp no és una classe; és una interfície. Això vol dir que quan la inicialtzem,
         * ho podem fer amb qualsevol classe que implementi Map.
         */
        private static Map<String,String> dades;
        
        // el mot reservat "final" evita que una variable pugui ser inicialitzada més d'un cop
        private final enum_Idiomes clau;
        private final enum_Idiomes valor;
        
        /***
         * Constructor.
         * Inicialitza un diccionari de traducció amb els dos idiomes seleccionats
         */
	public Model(){
                /***
                 * TreeMap és una implementació de Map amb una
                 * estructura interna d'arbre ordenat i que garanteix l'accès als 
                 * elements amb un rendiment optim (log n)
                 * https://danielmiessler.com/study/big-o-notation/
                 * 
                 */
		dades=new TreeMap<>();
                
                // definim la clau en CATALÀ
                this.clau = enum_Idiomes.CAT;
                
                // definim el valor en anglès
                this.valor = enum_Idiomes.ANG;
	}
	
	/**
         * Afegeix una paraula 
         * @param clau paraula en català
         * @param valor paraula en anglès
         */
        public void afegirParaula(String clau,String valor) {
		dades.put(clau, valor);
        }
	
        /**
         * Elimina l'entrada subministrada per la clau
         * @param clau
         * @return  true si ha pogt eliminar la paraula
         */
	public boolean eliminarParaula(String clau) {
            boolean ret = true;
            
            if (dades.containsKey(clau)) {
                dades.remove(clau);
            }else{
        	ret = false;
            }   
            
            return ret;
    }
    
    /**
     * Mostra la traducció o null si no ha trobat res
     * @param clau
     * @return 
     */
        public String cercarTraduccio(String clau){
            
            // el mètode get retorna null si no troba la clau
            String _valor = (String)dades.get(clau);
            
        return _valor; 
    }
    
    /**
     * Verifica si l'entrada subministrada ja esisteix 
     * @param clau clau
     * @return 
     */
    public boolean entradaDuplicada(String clau){
            return dades.containsKey(clau);
    }
    
    /**
     * Retorna les dades del diccionari
     * @return 
     */
    public Map<String,String> getDades() {
        return dades;
    }

    @Override
    public String toString() {
        return clau + " : "+ valor;
    }
    
    /***
     * Retorna el numero d'entrades del diccionari
     * @return 
     */
    public int totalEntrades()
    {
        return dades.size();
    }
}
